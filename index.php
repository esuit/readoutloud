<?php
header('Access-Control-Allow-Origin: http://localhost:3000');

function var_dump_ret($mixed = null) {
  ob_start();
  var_dump($mixed);
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}

try {
    error_log(var_dump_ret($_GET));
    $uri = $_GET['uri'];            // the URI that we're fetching

    if (is_null($_GET['uri'])) {
        throw new Exception('Bad URI');
    }

    $ch = curl_init("https://mercury.postlight.com/parser?url=".$uri);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER,
     array('Content-Type: application/json',
    'X-api-key:$API_KEY
    ));



    $contents = curl_exec($ch);
    $info = curl_getinfo($ch);

    if ($info['http_code'] >= 400) {
        throw new Exception('Bad error code (' . $info['http_code'] . ')');
    }

    // header shit.
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');

    // base64 encode the data
    //$data = base64_encode($contents);
    //echo $contents;
    echo json_encode($contents);//'{ "data" : "'. $data .'" }';
    //error_log($contents);
} catch (Exception $e) {
    http_response_code(500);
    echo "ERROR: " . $e->getMessage();
    error_log("ERROR: " . $e->getMessage());

    exit(1);
}



?>
